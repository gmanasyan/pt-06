package ru.manasyan.patterns.state.states;

import ru.manasyan.patterns.state.CopyContext;

public class NextStepState extends StateBase {

    @Override
    public void requestChange(CopyContext context) {
        context.state = new MoneyChangeState();
        context.state.requestChange(context);
    }

    @Override
    public void selectDocument(CopyContext context) {
        context.state = new DocumentState();
        context.state.selectDocument(context);
    }

}
