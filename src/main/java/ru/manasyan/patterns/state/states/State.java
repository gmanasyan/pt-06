package ru.manasyan.patterns.state.states;

import ru.manasyan.patterns.state.CopyContext;

public interface State {

    void addMoney(CopyContext context);

    void selectDevice(CopyContext context);

    void selectDocument(CopyContext context);

    void print(CopyContext context);

    void requestChange(CopyContext context);

}
