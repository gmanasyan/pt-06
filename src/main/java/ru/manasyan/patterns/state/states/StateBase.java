package ru.manasyan.patterns.state.states;

import ru.manasyan.patterns.state.CopyContext;
import ru.manasyan.patterns.state.Device;
import ru.manasyan.patterns.state.Document;

public abstract class StateBase implements State {

    @Override
    public void addMoney(CopyContext context) {
        notSupportedException();
    }

    @Override
    public void selectDevice(CopyContext context) {
        notSupportedException();
    }

    @Override
    public void selectDocument(CopyContext context) {
        notSupportedException();
    }

    @Override
    public void print(CopyContext context) {
        notSupportedException();
    }

    @Override
    public void requestChange(CopyContext context) {
        notSupportedException();
    }

    private void notSupportedException() {
        throw new RuntimeException("Operation not supported");
    }
}
