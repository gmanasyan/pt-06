package ru.manasyan.patterns.state.states;

import ru.manasyan.patterns.state.CopyContext;

public class PrintState extends StateBase {

    @Override
    public void print(CopyContext context) {
        System.out.println("### Print");
        System.out.println(context.getDocument().getFileContent());
        context.state = new NextStepState();
    }

}
