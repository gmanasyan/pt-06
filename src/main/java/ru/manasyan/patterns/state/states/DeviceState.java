package ru.manasyan.patterns.state.states;

import ru.manasyan.patterns.state.CopyContext;

public class DeviceState extends StateBase {

    @Override
    public void selectDevice(CopyContext context) {
        System.out.println("### Device set");
        System.out.println(context.getDevice().toString());
        context.state = new DocumentState();
    }
}
