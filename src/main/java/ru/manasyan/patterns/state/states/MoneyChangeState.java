package ru.manasyan.patterns.state.states;

import ru.manasyan.patterns.state.CopyContext;

public class MoneyChangeState extends StateBase {

    @Override
    public void requestChange(CopyContext context) {
        System.out.println("### Here is your change");
        System.out.println(context.getMoneyAmount());
    }
}
