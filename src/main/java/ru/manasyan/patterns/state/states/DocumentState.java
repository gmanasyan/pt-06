package ru.manasyan.patterns.state.states;

import ru.manasyan.patterns.state.CopyContext;

public class DocumentState extends StateBase {

    @Override
    public void selectDocument(CopyContext context) {
        System.out.println("### Document set");
        System.out.println(context.getDocument().getFileName());
        context.state = new PrintState();
    }

}
