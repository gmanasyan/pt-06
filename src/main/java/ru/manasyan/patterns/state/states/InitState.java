package ru.manasyan.patterns.state.states;

import ru.manasyan.patterns.state.CopyContext;

public class InitState extends StateBase {

    @Override
    public void addMoney(CopyContext context) {
        System.out.println();
        System.out.println("######## init stage ############");
        System.out.println("### Money added");
        System.out.println(context.getMoneyAmount());
        context.state = new DeviceState();
    }


}
