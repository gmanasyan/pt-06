package ru.manasyan.patterns.state;

public class Main {

    public static void main(String[] args) {
        CopyContext context = new CopyContext();
        context.addMoney(1000);
        context.selectDevice(Device.FLASH);
        context.selectDocument();
        context.print();
        context.requestChange();

        CopyContext context2 = new CopyContext();
        context2.addMoney(1000);
        context2.selectDevice(Device.FLASH);
        context2.selectDocument();
        context2.print();
        context2.selectDocument();
        context2.print();
        context2.requestChange();

        try {
            CopyContext context3 = new CopyContext();
            context3.addMoney(1000);
            context3.selectDevice(Device.FLASH);
            context3.print();
            context3.requestChange();
        } catch (Exception ex) {
            System.out.println("Incorrect step: " + ex.getMessage());
        }

    }
}
