package ru.manasyan.patterns.state;

import ru.manasyan.patterns.state.states.InitState;
import ru.manasyan.patterns.state.states.State;

public class CopyContext {

    public State state;

    private Document document;

    private Device device;

    private Integer moneyAmount;

    public CopyContext() {
        this.state = new InitState();
    }

    public void addMoney(Integer moneyAmount) {
        this.moneyAmount = moneyAmount;
        state.addMoney(this);
    }

    public void selectDevice(Device device) {
        this.device = device;
        state.selectDevice(this);
    }


    public void selectDocument() {
        this.document = new Document("file1.doc", "Content from file1");
        state.selectDocument(this);
    }

    public void print() {
        moneyAmount -= 500;
        state.print(this);
    }

    public void requestChange() {
        state.requestChange(this);
        resetState();
    }

    private void resetState() {
        this.state = new InitState();
        this.device = null;
        this.document = null;
        this.moneyAmount = 0;
    }

    public Document getDocument() {
        return document;
    }

    public State getState() {
        return state;
    }

    public Device getDevice() {
        return device;
    }

    public Integer getMoneyAmount() {
        return moneyAmount;
    }
}
