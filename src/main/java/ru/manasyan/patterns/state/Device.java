package ru.manasyan.patterns.state;

public enum Device {
    FLASH,
    WIFI
}
